var
  jsonServer = require('json-server'),
  jwt = require('jsonwebtoken'),

  server = jsonServer.create(),
  router = jsonServer.router('db.json'),
  middlewares = jsonServer.defaults(),
  port = 8084,
  dashboardUrlPart = '/dashboard/api/v1',
  authUrlPart = '/auth/api/v1';

server.use(middlewares);
server.use(jsonServer.bodyParser);

function login (req, res) {
  var token = generateToken(),
    login = req.body.login,
    password = req.body.password;

  if (login === 'test' && password === 'test1234') {
    res.jsonp({
      token: token
    });
  } else {
    res.sendStatus(401);
  }
}

function generateToken() {
  let payload = {
      foo: 'bar'
    },
    secret = 'secret';

  return jwt.sign(payload, secret);
}

function getToken(req) {
  var token = req.headers &&
    req.headers &&
    req.headers.authorization;

  if (token) {
    return token.split(' ')[1];
  }

  return null;
}

function isAuthorized(req) {
  var
    decoded,
    token = getToken(req);

  try {
    decoded = jwt.verify(token, secret);
  } catch(err) {
    // err
  }

  if (token) {
    return true;
  }
}

function getLoggedUser(req, res) {
  let http = require('http');
  let authorizationHeader = req.headers.authorization;

  http.request({
    port: 8084,
    path: `${dashboardUrlPart}/users/0`,
    method: 'GET',
    headers: {
      Authorization: authorizationHeader
    }
  }, (response) => {
    let data = '';

    response.on('data', function (chunk) {
      data += chunk;
    });

    response.on('end', function() {
      res.jsonp(JSON.parse(data));
    });
  }).end();
}

function putDisplayName(req, res) {
  let http = require('http');
  let authorizationHeader = req.headers.authorization;

  http.request({
    port: 8084,
    path: `${dashboardUrlPart}/users/0`,
    method: 'GET',
    headers: {
      Authorization: authorizationHeader
    }
  }, (response) => {
    let data = '';

    response.on('data', function (chunk) {
      data += chunk;
    });

    response.on('end', function() {
      let displayNameDto = {
        displayName: JSON.parse(data).displayName
      };

      res.jsonp(displayNameDto);
    });
  }).end();
}

function putEmail(req, res) {
  let http = require('http');
  let authorizationHeader = req.headers.authorization;

  http.request({
    port: 8084,
    path: `${dashboardUrlPart}/users/0`,
    method: 'GET',
    headers: {
      Authorization: authorizationHeader
    }
  }, (response) => {
    let data = '';

    response.on('data', function (chunk) {
      data += chunk;
    });

    response.on('end', function() {
      let tokenDto = {
        token: generateToken()
      };

      res.jsonp(tokenDto);
    });
  }).end();
}

function putPassword(req, res) {
  let http = require('http');
  let authorizationHeader = req.headers.authorization;

  http.request({
    port: 8084,
    path: `${dashboardUrlPart}/users/0`,
    method: 'GET',
    headers: {
      Authorization: authorizationHeader
    }
  }, (response) => {
    let data = '';

    response.on('data', function (chunk) {
      data += chunk;
    });

    response.on('end', function() {
      let passwordDto = {
        value: JSON.parse(data).password
      };

      res.jsonp(passwordDto);
    });
  }).end();
}

server.post(`${authUrlPart}/login`, login);
server.post(`${dashboardUrlPart}/login`, login);

server.get(`${dashboardUrlPart}/users/me`, getLoggedUser);
server.put(`${dashboardUrlPart}/users/me`, putDisplayName);
server.put(`${dashboardUrlPart}/users/me/email`, putEmail);
server.put(`${dashboardUrlPart}/users/me/password`, putPassword);

server.use(function (req, res, next) {
  if (isAuthorized(req)) {
    next();
  } else {
    res.sendStatus(401);
  }
});

server.use('/dashboard/api/v1/companies', router);
server.use('/dashboard/api/v1', router);
server.use(router);
server.listen(port, function () {;
  console.log(`JSON Server is running at port: http://localhost:${port}`);
});
