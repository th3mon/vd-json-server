# Videolicious Dashboard Fake REST Server

## Project uses:
  - [JSON Server](https://github.com/typicode/json-server)
  - [JsonWebToken implementation for node.js](https://github.com/auth0/node-jsonwebtoken)
  - [Nodemon](https://nodemon.io/)

## Run

npm start
